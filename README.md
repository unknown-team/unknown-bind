# unknown-alpine
An image using AlpineLinux that runs bind9.

## Notes
- IPv4 only
- Should be run with a read-only RootFS
- Configuration file must be mounted at ```/etc/bind/named.conf```
- Zones must be mounted at ```/var/named/```
