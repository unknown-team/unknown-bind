FROM unknown-alpine:3.6
LABEL maintainer Alexander 'Polynomdivision'

# Install bind
RUN apk add --no-cache bind

# Add a user that runs bind
RUN addgroup -g 1004 bind \
    && adduser -H -D -u 1004 -G bind bind

# Start the bind daemon
# -f: Foreground
# -4: IPv4 only
ENTRYPOINT ["su-exec", "bind", "/usr/sbin/named", "-f", "-4"]